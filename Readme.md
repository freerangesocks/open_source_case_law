This is an tool created to make the legal research within the realm of Family Law easier for pro-se litigants.
While not elegant and complete in its access this repository and its contents leverages opensource law resources ,
in an effort to provide the user relevant resources based on the user input query.

This is an ongoing effort and I will continue to improve upon the functionality of this project and also extend from Chicago, to other cities and states as time and resources permit.

If you find value or want to contribute : https://paypal.me/soldiersandsaints?country.x=US&locale.x=en_US

email me here ns@soldiersandsaints.com

Enjoy the app
